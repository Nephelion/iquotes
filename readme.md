## Acceso ##
Se puede acceder a iQuotes en el siguiente dominio: [iquotes.live](http://iquotes.live)

## Estructura de la aplicación ##

iQuotes tiene una estructura muy modular. El archivo principal, app.js, constituye el núcleo del backend. En él se importan todas las librerías utilizadas, se configuran las routes, la base de datos, el puerto a utilizar, se incluye el middleware, se declaran variables globales... Ahora examinemos cada uno de los módulos de los que se compone iQuotes. Por orden alfabético:

**config**: es un módulo auxiliar donde encontramos dos archivos. El primero, database.js, nos ayuda a determinar qué base de datos utilizar en función de si ejecutamos la aplicación en un puerto local o en un servidor remoto. El segundo, passport.js, exporta una función que contiene una estrategia local de autenticación de la librería de JS Passport.

**helpers**: este es otro módulo auxiliar en el que solo encontramos un archivo, auth.js. En él exportamos una función que nos permite restringir determinadas routes a los usuarios que todavía no están autenticados.

**models**: este es un módulo central de la aplicación. En él encontramos los modelos de las colecciones que vamos a usar en nuestra base de datos: Idea.js, para almacenar las citas, y Users.js, para almacenar los usuarios.

**public**: este módulo está destinado a contener archivos estáticos, como imágenes u archivos css.

**routes**: este es otro módulo muy importante. En él construimos las routes relativas a la publicación y presentación de citas, por un lado, y al registro y autenticación de usuarios, por otro.

**views**: este módulo quizás sea el más importante de todos. En él está prácticamente todo el _front-end_ de iQuotes. Aquí nos encontramos en primer lugar dos archivos de extensión .handlebars. El primero, index, contiene el template con los elementos gráficos de la página de inicio. El segundo, about, los elementos gráficos que conforman la route '/about'.

Pasamos ahora a las subcarpetas de *views*. La primera, _ideas_, contiene tres archivos de extensión .handlebars. El primero, add.handlebars, contiene los elementos gráficos que 'vemos' a la hora de añadir una nueva cita (formulario, botón etc.). El segundo, edit.handlebars, es muy parecido al anterior, pero en este caso para editar citas. El tercer archivo, index, codifica la la presentación de las citas una vez han sido publicadas.

La siguiente subcarpeta es _layouts_, donde está el archivo main.handlebars, que constituye el 'esqueleto' del front-end de iQuotes. Digamos que codifica la presentación de las partes de la aplicación que se ven en todo momento, al margen de la route en la que nos encontremos.

Pasamos a _partials_, que contiene fragmentos de la presentación de la aplicación que luego se importan en el archivo main.handlebars. Aquí nos encontramos 6 archivos. El primero es errors.handlebars, donde codificamos la presentación de mensajes de error emergentes que aparecen en diversas contingencias, como, por ejemplo, cuando introducimos una contraseña incorrecta para autenticarnos. El segundo, head.handlebars contiene parámetros generales del front-end y enlaza _style-sheets_ al proyecto. El siguiente archivo, msg.handlebars, es igual que el primero pero con mensajes emergentes de 'éxito', que aparecen, por ejemplo, para informarte de que la cita que has añadido se ha publicado correctamente. El siguiente archivo, navbar.handlebars, codifica la presentación de la barra de navegación principal de la aplicación, donde podemos encontrar el enlace a todas las partes y funcionalidades de iQuotes. El archivo scripts.handlebars importa diversas tecnologías como jQuery o Bootstrap. Por último, el archivo style.handlebars equivale a una style-sheet de css, donde se editan todas las clases y componentes que conforman las diferentes vistas de la aplicación.

Por último, la última subcarpeta de *views* es _users_, donde encontramos dos archivos, login.handlebars codifica la presentación del formulario que usamos para iniciar sesión, y register.handlebars, la del formulario de registro.

## Tecnologías empleadas ##

NodeJS, ExpressJS, MongoDB, Bootstrap, HTML, CSS, Heroku, Mlab.